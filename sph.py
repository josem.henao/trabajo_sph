0#===========================================================================================================
#0_LIBRERIAS ADICIONALES NECESARIAS
from numpy import *
from matplotlib.pyplot import *

#===========================================================================================================
#01_POSICIONES INICIALES DE LAS PARTICULAS
#Partículas virtuales:  tipo 1 (frontera rígida), tipo 2 (afuera de la frontera )

nx=40 #numero puntos en x
ny=40 #numero de puntos en y
nTotal=nx*ny #numero de particulas total
Lx=0.001 #longitud total en x
Ly=0.001 #longitud total en y

dx=Lx/(nx)
dy=Ly/(ny)

print("dx =",dx,"- dy =",dy)

#===========================================================================================================
#02_CREAR EL GRID
X_init=0.0 #posicion inicial en x para el grid
Y_init=0.0 #posicion inicial en y para el grid

px=arange(X_init + dx/2.0, X_init + nx*dx, dx, dtype=float)
py=arange(Y_init + dy/2.0, Y_init + ny*dy, dy, dtype=float)
#arange(inicio,final,paso) (solo funciona con numpy)
#partículas reales no deben coincidir con partículas virtuales por eso se pone dx/2
#i.e no esten en el mismo punto

print(len(px),px[0],px[-1]) #imprime longitud puntos x, el primero y el último

#===========================================================================================================
#03_ALMACENAR POSICIONES DE PARTICULAS EN SIMULACIÓN (reales y virtuales)
#Se almacenan en listas vacías
x=[] #arreglo con las posiciones en x
y=[] #arreglo con las posiciones en y

#NOTA: Tengo un arreglo para x, para y, para la masa, para la densidad
#y al final se llama los arreglos con los indices que serian los mismos para los arreglos
#(así se facilita el trabajo)

#===========================================================================================================
#04_GUARDAR LOS VALORES EN ARREGLO
#Se crean 2 vectores para  no manipular matrices que es más complicado
for i in range(nx):
    for j in range(ny):
        #print(i,j,px[i],py[j])
        x.append(px[i])
        y.append(py[j])
    #tengo una lista de x y y

#(x[0],y[0])=Px[0],Py[0] con un solo índice se puede conocer toda la información del punto.

#===========================================================================================================
#05_GRAFICA
#por defecto gráfica en linea punto (plot)
#entonces se le debe decir que grafique solo con puntos (scatter) y darle un marker

figure(figsize=(10,10)) #para que grafique cuadrado
scatter(x,y,s=10,marker=".") #marker="." #para que haga la grafica con puntos y s=1
xlim(0.0-dx,Lx+dx)
ylim(0.0-dy,Ly+dy)
#show()

#===========================================================================================================
#06_CAMPO DE VELOCIDADES
vx=[] #arreglo con las velocidades en x
vy=[] #arreglo con las velocidades en y

for i in range(nx):
    for k in range(ny): #los : son para que corra el for
        #están igualadas a cero porque el flujo es estacionario al principio
        vx.append(0.0)
        vy.append(0.0)

rho=[] #densidad
m=[] #masa
p=[] #presion
u=[] #energía cinética
h=[] #longitud de suavizado
c=[] #velocidad del sonido
tipo=[] #tipo de particula

for i in range(nTotal):
    rho.append(1000)
    m.append(dx*dy*rho[i])
    p.append(0.0)
    u.append(357.1)
    h.append(dx)
    c.append(0.0)
    tipo.append(1)
    """1 porque son partículas del fluido y -1 partículas de la frontera"""

#partículas virtuales tipo I para la frontera
V_top=1e-3
npVirt=320
npV=int(320/4) #convierte en numero entero

#particulas de la frontera SUPERIOR
X_ini=0.0
px=arange(X_ini,X_ini + (npV*(dx/2.0)) + (dx/2.0), dx/2.0, dtype=float)

xVIup=[]
yVIup=[]
for i in range(len(px)):
    x.append(px[i])
    y.append(Ly)
    vx.append(V_top)
    vy.append(0.0)
    xVIup.append(px[i])
    yVIup.append(Ly)
    tipo.append(-1)
print(len(xVIup))

#Particulas de la frontera DERECHA
Y_ini= 0.0
py=arange(Y_ini + dy/2.0, Y_ini + (npV*dy/2.0), dy/2.0, dtype=float) #Duda

xVIrg=[]
yVIrg=[]
for i in range(len(py)):
    x.append(Lx)
    y.append(py[i])
    vx.append(0.0) #dudo
    vy.append(-1e-3)
    xVIrg.append(Lx)
    yVIrg.append(py[i])
    tipo.append(-1)
print(len(xVIrg))
   
#Particulas de la frontera INFERIOR
X_ini=0.0
px=arange(X_ini,X_ini + (npV*dx/2.0) + (dx/2.0), dx/2.0, dtype=float)

xVIbt=[]
yVIbt=[]
for i in range(len(px)):
    x.append(px[i])
    y.append(0.0)
    vx.append(-1e-3)
    vy.append(0.0)
    xVIbt.append(px[i])
    yVIbt.append(0.0)
    tipo.append(-1)
print(len(xVIbt))

#particulas de la frontera IZQUIERDA
Y_ini=0.0
py=arange(Y_ini + dy/2.0,Y_ini + (npV*dy/2.0) + (dy/2.0), dx/2.0, dtype=float)

xVIlf=[]
yVIlf=[]
for i in range(len(py)):
    x.append(0.0)
    y.append(py[i])
    vx.append(0.0)
    vy.append(1e-3)
    xVIrg.append(0.0)
    yVIrg.append(py[i])
    tipo.append(-1)
print(len(xVIlf))

print(len(xVIup)+len(xVIrg)+len(xVIbt)+len(xVIlf),"particulas virtuales")

#pp adicionales de particulas virtuales
for i in range(npVirt):
    rho.append(1000)
    m.append(dx*dy*rho[i])
    p.append(0.0)
    c.append(0.0)
    u.append(357.1)
    h.append(dx/2)
    
#Gráfica de posiciones
figure(figsize=(10,10))
scatter(x,y,s=20,marker='.')
scatter(xVIup,yVIup,s=20,marker='.',c='r')
scatter(xVIrg,yVIrg,s=20,marker='.',c='g')
scatter(xVIbt,yVIbt,s=20,marker='.',c='y')
scatter(xVIlf,yVIlf,s=20,marker='.',c='b')

xlabel("X")
ylabel("Y")
xlim(0.0-dx,Lx+dx) #limite del grafico
ylim(0.0-dy,Ly+dy) #+dx para ver el cuadro completo
tight_layout() #elimina innecesarias
#show()

#puntos azules: fluido
#puntos otros colores: fronteras

#grafica campo de velocidad
figure(figsize=(10,10))
quiver(x, y, vx, vy, scale=0.1) #grafica vectores
xlabel("X")
ylabel("Y")
xlim(0.0-dx,Lx+dx)
ylim(0.0-dy,Ly+dy)
tight_layout()
#show()

#===========================================================================================================
#07_BUSQUEDA DE VECINOS: no con optimizado sino con distancias
def W(r,h): #kernel entre particula i y j

    R=r/h
    alpha=15.0/(7.0*pi*h*h)

    if (R>=0.0) and (R<=1.0):
        return alpha*((2.0/3.0)-R*R+0.5*R*R*R)
    elif (R>1.0) and (R<=2.0):
        return alpha*((1.0/6.0)*(2.0-R)*(2.0-R)*(2.0-R))
    else:
        return 0.0

def dW(r,dx,h):

    R=r/h
    alpha=15.0/(7.0*pi*h*h)

    if (R>=0.0) and (R<=1.0):
        return alpha*(-2.0+1.5*R)*dx/(h*h)
    elif (R>1.0) and (R<=2.0):
        return alpha*(-0.5*(2.0-R)*(2.0-R))*dx/(h*h*R)
    else:
        return 0.0

#Test para kernel

R=[]
kernel=[]
dkernel=[]

l=list(range(-30,30+1,1))
for i in l:
    R.append(i/10.0)
    kernel.append(W(abs(R[i+30]),1.0))
    dkernel.append(dW(abs(R[i+30]),R[i+30]/sqrt(3),1.0)) #h=1.0

fs=15 #definir el tamaño de la letra
figure(figsize=(10,10))
plot(R,kernel,ms=10,marker='.',label="kernel")
#plot--> curvas; scatter-->puntos
plot(R,dkernel,ms=10,marker='.',label="dkernel")
grid()
xlabel("R [m]", fontsize=fs) #las unidades en corchetes
ylabel("Funciones",fontsize=fs)
xlim(-3.0,3.0)
ylim(-1.0,1.0)
legend(fontsize=fs)
title("Test kernel",fontsize=fs)
#show()

#===========================================================================================================
#08_BUSQUEDA DE VECINOS y calculo de distancia distancia directa
#vecinos son las particulas que estan en dominio de soporte
#cuanto vale kernel entre cada particula i y j
def NNPS(i,kappa,NPart): #i=particula
    nn=[] #indice(arreglo) de vecinos
    d=[] #vecinos
    dx=[] #distancia x
    dy=[] #distancia y
    Wij=[] #kernel entre particulas i y j
    dWijx=[] #derivada del kernel en x a c/vec
    dWijy=[] #derivada del kernel en y a c/vec
    #derivada del kernel por componentes para poder calcular
    #divergencias,rotacionales y XX cuando se necesite

    for j in range(NPart):
        if j!=i: #no tiene sentido cuando j=i ent j dif de i
            xij=x[i]-x[j]
            yij=y[i]-y[j]
            r=(sqrt(xij*xij+yij*yij)) #distancia entre 2 puntos
            hij=0.5*(h[i]+h[j]) #longitud de suavizado promedio
            #hij medio aritmetica de las particulas de suavizado
            if r<kappa*hij: #dominio de soporte
                nn.append(int(j))
                dx.append(xij)
                dy.append(yij)
                d.append(r)
                Wij.append(W(r,hij))
                dWijx.append(dW(r,xij,hij))
                dWijy.append(dW(r,yij,hij))

    return nn,d,Wij,dWijx,dWijy

#test para la busqueda de vecinos
import random

N=nx*ny+npVirt #numero total de particulas para busq de vecinos
kappa=2.0

figure(figsize=(10,10))
scatter(x,y,s=10,marker='.')  #x contiene part de fluido y frontera en x
for pt in range(10):
    i=random.randrange(nTotal)
    print("\nVecinos para particula",i)
    nn, r, Wij, dWijx, dWijy =NNPS(i,kappa,N) #vecinos xa c/part
    for vecinos in range(len(nn)): #donde vecinos es un contador
        print(nn[vecinos],tipo[nn[vecinos]])   
    scatter(x[i],y[i],s=10,marker='.',c='r') #se le agregan coordenadas
    xnn=[] #lista vacia en x para vecinos
    ynn=[]
    for j in range(len(nn)): #num vecinos de part i
        xnn.append(x[nn[j]]) #coordenadas de los vecinos
        ynn.append(y[nn[j]])
    scatter(xnn,ynn,s=10,marker='.',c='k')
    xlim(0.0-dx,Lx+dx)
    ylim(0.0-dy,Ly+dy)
#show()

    #en rojo la particula de interes
    #en negro las que se consideran vecinas
    #en azul el resto de part fluido

#===========================================================================================================
#09_CAMPO DE DENSIDAD
#densidad de suma normalizada
#solo se le calcula a part fluido

def densidad(): #no tiene parametros

    #primeros vecinos
    kappa=2.0 #longitud de suavizado + dominio soporte
    NN=[] #numero vecinos
    #NN es super lista con las listas de todos los vecinos de las particulas
    r=[] #distancia de los vecinos
    Wii=[] #contribucion de la part sobre si misma
    Wij=[]
    dWijx=[]
    dWijy=[]

    #Calcular los vecinos
    #el kernel se calcula aqui para poder llamarlo despues
    for i in range(nTotal): #nTotal= es solo el fluido
        nn, d, wij, dwijx, dwijy =NNPS(i,kappa,nTotal+npVirt)
        # +npVirt --> se incluye parti frontera como vecinos
        #lista de lista
        #MAYUS=
        #MINS=valores que retorna la funcion 
        NN.append(nn)
        r.append(d)
        Wij.append(wij)
        dWijx.append(dwijx)
        dWijy.append(dwijy)
        Wii.append(W(0.0,h[i])) #autokernel

    print("Vecinos calculados")
    
    #Calcular la densidad
    #calculo de las pp de particulas que evolucionan en t
    #i.e a las virtuales no se les calcula eso

    #numerador
    for i in range(nTotal): 
        den=m[i]*Wii[i] #sumatoria de masa por kernel primero
        for k in range(len(NN[i])): 
            #sumatoria sobre factor de normalizacion despues
            j=NN[i][k]
            den=den+m[j]*Wij[i][k]
        rho[i]=den

    #denominador
    #normalizando la densidad
    #primer for: controla todas las particulas
    #segundo for: con vecinos
    for i in range (nTotal):
        norm=(m[i]/rho[i])*Wii[i]
        for k in range(len(NN[i])):
            j=NN[i][k]
            norm=norm+(m[j]/rho[j])*Wij[i][k]
        rho[i]= rho[i]/norm
        #actualiza el arreglo de densidades "rho"

    print("Densidad calculada")

    return NN,rho,Wij,dWijx,dWijy
    #se retorna para poder utilizarlo en otra función
    #porque c/funcion se comparta como programas diferentes, ailsados    

#===========================================================================================================
#10_ACELERACIÓN Y ENERGÍA
#fluido incompresible
#ecc de movimiento

def navierStokes(NN,rho,dWijx,dWijy):

    #calculo presion
    def eos(rho,c):return c*c*rho

    for i in range(nTotal+npVirt):
        c[i]=0.01 #debe estar inicializada en part de fluidos y part virtuales
        p[i]=eos(rho[i],c[i])

    print("Presión calculada")

    #calculo aceleración y cambio temporal de energía
    dvxdt=[]
    dvydt=[]
    dedt=[] #cambio energía

    #euler
    for i in range(nTotal): #solo evolucionan las particulas del fluido
        ax=0.0
        ay=0.0
        de=0.0
        for k in range(len(NN[i])):
            j=NN[i][k]
            pij=(p[i]/(rho[i]*rho[i]))+(p[j]/(rho[j]*rho[j])) #termino presiones
            vdW=(vx[i]-vx[j])*dWijx[i][k]+(vy[i]-vy[j])*dWijy[i][k] #producto punto
            ax=ax-m[j]*pij*dWijx[i][k]
            ay=ay-m[j]*pij*dWijy[i][k]
            de=de+0.5*m[j]*pij*vdW
        dvxdt.append(ax)
        dvydt.append(ay)    
        dedt.append(de)

    print("Aceleracion calculada")
    print("Cambio temporal de energía calculado")
    return dvxdt, dvydt, dedt

#===========================================================================================================
#11_MODELO DE VISCOCIDAD ARTIFICIAL
def viscosidad(NN,rho,dWijx,dWijy,dvxdt,dvydt,dedt):

    #constantes empiricas
    alphapi=1.0
    betapi=1.0
    H=dx
    eps=0.01*H*H #evitar error por part demasiado cercanas

    for i in range(nTotal):
        for k in range(len(NN[i])):
            j=NN[i][k]
            rijx=x[i]-x[j] #resta de posiciones en x con vecinos 
            rijy=y[i]-y[j]
            vijx=vx[i]-vx[j] #resta de vel con vecinos
            vijy=vy[i]-vy[j]
            vijrij=vijx*rijx+vijy*rijy
            if vijrij<0.0:
                hij=0.5*(h[i]+h[j])
                phiij=(hij*vijrij)/(rijx*rijx+rijy*rijy+eps)
                cij=0.5*(c[i]+c[j])
                rhoij=0.5*(rho[i]+rho[j])
                Piij=(-alphapi*cij*phiij+betapi*phiij*phiij)/(rhoij) #tensor
            else:
                Piij=0.0 #si no hay compresion en el flujo

            #adicionando viscodidad a las ecc de movimiento
            dvxdt[i]=dvxdt[i]+(-m[j]*Piij*dWijx[i][k])
            dvydt[i]=dvydt[i]+(-m[j]*Piij*dWijy[i][k])
            vdW=(vx[i]-vx[j])*dWijx[i][k]+(vy[i]-vy[j])*dWijy[i][k]
            dedt[i]=dedt[i]+0.5*m[j]*Piij*vdW

    print("Viscosidad calculada")
    return 1

#===========================================================================================================
#12_INTERACCIÓN CON FRONTERA
#part de fluidos experimenten fuerza de repulsión para que no se salgan
#ent solo se aplica si hay particulas tipo -1

def frontera(NN, dvxdt, dvydt):

    #constantes de la fuerza de repulsión
    r0=dx/2.0
    n1=12
    n2=4
    D=0.01

    for i in range(nTotal):
        for k in range(len(NN[i])):
            j=NN[i][k]
            if tipo[j]==-1:
                xij=x[i]-x[j]
                yij=y[i]-y[j]
                rij=sqrt(xij*xij+yij*yij)
                if rij<r0:
                    PBxij=D*((r0/rij)**n1-(r0/rij)**n2)*(xij/(rij*rij))
                    PByij=D*((r0/rij)**n1-(r0/rij)**n2)*(yij/(rij*rij))
                else:
                    PBxij=0.0
                    PByij=0.0
                dvxdt[i]=dvxdt[i]+PBxij
                dvydt[i]=dvydt[i]+PByij

    print("Interacción con fronteras")
    return 1

#===========================================================================================================
#13_VELOCIDAD PROMEDIO
#se considera vel c/par para suavizar campo
#epsilon que importacia tiene el promedio en el sistema

def velPromedio(NN,Wij):
    epsilon=0.3
    for i in range(nTotal):
        vMeanx=0.0 #promedio en x
        vMeany=0.0 #promedio en y
        for k in range(len(NN[i])):
            j=NN[i][k]
            vijx=vx[i]-vx[j]
            vijy=vy[i]-vy[j]
            rhoij=0.5*(rho[i]+rho[j])
            vMeanx=vMeanx+(m[j]/rhoij)*vijx*Wij[i][k]
            vMeany=vMeany+(m[j]/rhoij)*vijy*Wij[i][k]
        vx[i]=vx[i]-epsilon*vMeanx
        vy[i]=vy[i]-epsilon*vMeany
    return 1

#===========================================================================================================
#14_EVOLUCIÓN TEMPORAL DEL FLUIDO

#Euler y demas no son reversible en tiempo ni conservativo
#Leap-frog:es un método de integración bueno y eficiente para cant de part utilizada
#integrador simpléctico: conservan la energía

#Para garantizar la conservación de la energía:
#Paso 1=evoluciona posicion "r" en mitad intervalo dt/2 con vel actual
#paso 2=evoluciona velocidad "v" en intervalo completo dt con aceleracion actual antes de haber evolucionado
#Paso 3=evoluciona posicion "r" la otra mitad del intervalo dt/2 con vel actualizada calc en 2
#-->P3:con posicion actual y velocidad que se acabo de calcular (velocidad actualizada)

def integracion(dt,dvxdt,dvydt,dedt):
    
    for i in range(nTotal):
        #paso 1
        x[i]=x[i]+0.5*dt*vx[i]
        y[i]=y[i]+0.5*dt*vy[i]
        #paso 2
        vx[i]=vx[i]+dt*dvxdt[i]
        vy[i]=vy[i]+dt*dvydt[i]
        #paso 3
        x[i]=x[i]+0.5*dt*vx[i]
        y[i]=y[i]+0.5*dt*vy[i]
        #evolucion de energia en un paso 
        u[i]=u[i]+dt*dedt[i]

    return 1

#===========================================================================================================
#15_FUNCION IMPRIME ESTADO

def imprime(counter):

    #abre archivo
    #open es ambiente de archivo
    #w= escribir archivo
    #r= leer archivo
    #./ el archivo queda en carpeta donde esta parado
    #.4d que imprima el entero con 4 digitos para quede orden
    estado=open("./salida/estado_%.4d.dat"%(counter),"w")

    #escribe datos en archivo
    for i in range(nTotal+npVirt):
        #archivo con columnas
        estado.write("%.3d %.8f %.8f %.8f %.8f %.8f %.8f %.8f\n"%(i,x[i],y[i],vx[i],vy[i],rho[i],p[i],u[i]))

    #cierra archivo
    estado.close()

    return 1

#===========================================================================================================
#16_SIMULACION
t=0.0
dt= 5e-5
tTotal=4000*dt
counter=0 #cuenta la cantidad de pasos del tiempo

print("\nINICIA LA SIMULACIÓN")

#Imprime estado inicial
imprime(counter) 

#Ciclo temporal
while t<=tTotal:
    print("\nTiempo de simulación t= %.8f"%t,"s - paso= %d"%counter,"\n")
    #.8 es el numero de decimales a imprimir
    #f es flotante (real)
    #d es entero
    #s es caracter
    #a escriba debajo de lo que ya hay pero necesitamso es que se reescriba
    #despues de % busca la variable del tipo que se le diga

    #Nota: cálculos dentro del ciclo para que imprima 1 a 1

    #_1_Cálculo de densidad(incluye busqueda vecinos)
    #se debe llamar porque arriba solo se esta definiendo
    #debido a que funciones son bloques de código independiente
    NN,rho,Wij,dWijx,dWijy=densidad()

    #_2_Cálculo de aceleracion y energia
    #inicializando presion y vel. del sonido
    #debajo de densidad porque la necesita
    dvxdt, dvydt, dedt=navierStokes(NN,rho,dWijx,dWijy)

    #_3_Cálculo de la viscosidad artificial
    viscosidad(NN,rho,dWijx,dWijy,dvxdt,dvydt,dedt)

    #_4_Interacción con fronteras
    frontera(NN,dvxdt,dvydt)

    #_5_Cálculo de la velocidad promedio
    velPromedio(NN,Wij)

    #_6_Integración
    integracion(dt,dvxdt,dvydt,dedt)

    #------------
    counter += 1
    t += dt
    
    #Imprime estado (siempre al final)
    imprime(counter)
    
else: #del while
    print("\nSimulación terminada")
#===========================================================================================================
